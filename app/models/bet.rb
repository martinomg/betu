class Bet < ActiveRecord::Base
	belongs_to :user 

	has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>" }

	
	validates :bet, presence: true
	validates :bet, length: { maximum:100 }

	validates :description, presence: true
	validates :description, length: { maximum:140 }

end
