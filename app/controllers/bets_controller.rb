class BetsController < ApplicationController
  before_action :set_bet, only: [:show, :edit, :update, :destroy]
  before_action :correct_user, only: [:edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show]

  def index
    @bets = Bet.all.order("Created_at DESC").paginate(:page => params[:page], :per_page => 12)
  end

  def show
  end

  def new
    @bet = current_user.bets.build
  end

  def edit
  end

  def create
    @bet = current_user.bets.build(bet_params)

      if @bet.save
        redirect_to @bet, notice: 'Bet was successfully created.'
      else
        render action: 'new'
      end
  end

  def update
      if @bet.update(bet_params)
        redirect_to @bet, notice: 'Bet was successfully updated.'
      else
        render action: 'edit'
      end
  end

  def destroy
    @bet.destroy
      redirect_to bets_url
      head :no_content
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bet
      @bet = Bet.find(params[:id])
    end

    def correct_user
      @bet = current_user.bets.find_by(id: params[:id])
      redirect_to bets_path, notice: "Not authorized to edit this bet" if @bet.nil?
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bet_params
      params.require(:bet).permit(:bet, :description, :image)
    end
end
