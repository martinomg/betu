class CreateBets < ActiveRecord::Migration
  def change
    create_table :bets do |t|
      t.string :bet
      t.string :description

      t.timestamps
    end
  end
end
