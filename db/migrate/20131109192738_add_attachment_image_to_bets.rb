class AddAttachmentImageToBets < ActiveRecord::Migration
  def self.up
    change_table :bets do |t|
      t.attachment :image
    end
  end

  def self.down
    drop_attached_file :bets, :image
  end
end
